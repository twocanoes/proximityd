//
//  AppDelegate.m
//  Proximityd Menu Item
//
//  Created by Tim Perfitt on 11/21/15.
//  Copyright © 2015 Twocanoes Labs. All rights reserved.
//

#import "AppDelegate.h"
@interface AppDelegate ()
@property (nonatomic,strong) NSStatusBar *bar;
@property (nonatomic,strong) NSStatusItem *theItem;
@property (weak) IBOutlet NSMenu *statusMenu;
@property (weak) IBOutlet NSMenu *theMenu;

@property (weak) IBOutlet NSWindow *window;
@end
static void Callback(CFNotificationCenterRef center,
                     void *observer,
                     CFStringRef name,
                     const void *object,
                     CFDictionaryRef inUserInfo)
{
    
    NSString *stateFile=@"/Library/Application Support/Twocanoes/Proximityd/state.plist";
    BOOL isDir;
    
    NSDictionary *event;
    if ([[NSFileManager defaultManager] fileExistsAtPath:stateFile isDirectory:&isDir] && isDir==NO) {
        
        event=[NSDictionary dictionaryWithContentsOfFile:stateFile];
    }
    else {
        return;
    }
    if (!event) return;

    NSString *description=[event objectForKey:@"description"];
    AppDelegate *delegate=(__bridge AppDelegate *)observer;
    NSMenu *statusBarItem=[delegate theMenu];

    NSMenuItem *inMenuItem=[statusBarItem  itemWithTag:0];
    NSMenuItem *outMenuItem=[statusBarItem  itemWithTag:1];
    
    NSMenu *inMenuSubmenu=[inMenuItem submenu];
    if (inMenuSubmenu==nil) {
        
        inMenuSubmenu=[[NSMenu alloc] initWithTitle:@"subIn"];
        inMenuItem.submenu=inMenuSubmenu;
    }
    
    NSMenu *outMenuSubmenu=[outMenuItem submenu];
    if (outMenuSubmenu==nil) {
        
        outMenuSubmenu=[[NSMenu alloc] initWithTitle:@"subOut"];
        outMenuItem.submenu=outMenuSubmenu;

    }

    NSMenuItem *itemToRemove;
    NSString *regionName=event[@"beaconInfo"][@"RegionName"];
    if ((itemToRemove=[inMenuSubmenu itemWithTitle:regionName])) {
        
        [inMenuSubmenu removeItem:itemToRemove];
        
    }
    if ((itemToRemove=[outMenuSubmenu itemWithTitle:regionName])) {
        
        [outMenuSubmenu removeItem:itemToRemove];

        
    }
    
    NSMenuItem *newItem=[[NSMenuItem alloc] initWithTitle:regionName action:nil keyEquivalent:@""];
    if ([event[@"event"] isEqualToString:@"enter"]){
        [inMenuSubmenu addItem:newItem];
    }
    
    else if ([event[@"event"] isEqualToString:@"exit"]){
        [outMenuSubmenu addItem:newItem];
       
   }
    
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"shouldNotify"] boolValue]==YES){
        NSUserNotification *notification = [[NSUserNotification alloc] init];
        notification.title =@"Proximityd Notification";
        notification.informativeText = description;
        notification.soundName = NSUserNotificationDefaultSoundName;
        [[NSUserNotificationCenter defaultUserNotificationCenter] deliverNotification:notification];

    }
    NSInteger inCount=[inMenuSubmenu numberOfItems];
    NSInteger outCount=[outMenuSubmenu numberOfItems];
    
    [inMenuItem setTitle:[NSString stringWithFormat:@"In Region (%li)",inCount]];
    [outMenuItem setTitle:[NSString stringWithFormat:@"Out of Region (%li)",outCount]];

    inMenuItem.enabled=YES;
    outMenuItem.enabled=YES;


}
@implementation AppDelegate

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification {
    
    [[NSUserDefaults standardUserDefaults] registerDefaults:@{@"shouldNotify":@YES}];
    CFNotificationCenterRef distributedCenter =
    CFNotificationCenterGetDarwinNotifyCenter();
    
    CFNotificationSuspensionBehavior behavior =
    CFNotificationSuspensionBehaviorDeliverImmediately;
    
    CFNotificationCenterAddObserver(distributedCenter,
                                    (void *)self,
                                    Callback,
                                    CFSTR("com.twocanoes.proximityd"),
                                    CFSTR("com.twocanoes.proximityd"),
                                    behavior);

    [self activateStatusMenu:self];
}

- (void)applicationWillTerminate:(NSNotification *)aNotification {
    // Insert code here to tear down your application
}
- (void)activateStatusMenu:(id)sender
{
    
    
    if (self.theItem == nil) {
        //NSMenuItem *nameserverMenuItem;
        self.bar = [NSStatusBar systemStatusBar];
        
        self.theItem = [self.bar statusItemWithLength:NSVariableStatusItemLength];
        [self.theItem setImage:[NSImage imageNamed:@"menuitem"]];
        [self.theItem setHighlightMode:NO];
        [self.theItem setMenu:self.theMenu];
         // kickoffnotification();
        
    }
    
}
- (void)deactivateStatusMenu:(id)sender
{
    if (self.theItem != nil) {
        [self.bar removeStatusItem:self.theItem];
        self.theItem=nil;
    }
}


@end
