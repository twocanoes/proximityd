//
//  AppDelegate.h
//  Proximityd Menu Item
//
//  Created by Tim Perfitt on 11/21/15.
//  Copyright © 2015 Twocanoes Labs. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface AppDelegate : NSObject <NSApplicationDelegate>


@end

