//
//  main.m
//  Proximityd Menu Item
//
//  Created by Tim Perfitt on 11/21/15.
//  Copyright © 2015 Twocanoes Labs. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, const char * argv[]) {
    return NSApplicationMain(argc, argv);
}
