//
//  TCLBeaconScanner.h
//  Bleu Broadcaster
//
//  Created by Tim Perfitt on 5/15/15.
//  Copyright (c) 2015 Twocanoes Software. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreBluetooth/CoreBluetooth.h>
#define TCLOGDEBUG(format,...) { asl_log(NULL, NULL, ASL_LEVEL_DEBUG, "%s", [[NSString stringWithFormat:format, ##__VA_ARGS__] UTF8String]);\
}

#define TCLOGINFO(format,...) {asl_log(NULL, NULL, ASL_LEVEL_INFO, "%s", [[NSString stringWithFormat:format, ##__VA_ARGS__] UTF8String]); \
}
#define TCLOGWARNING(format,...) { asl_log(NULL, NULL, ASL_LEVEL_WARNING, "%s", [[NSString stringWithFormat:format, ##__VA_ARGS__] UTF8String]); \
}

@interface TCLBeaconScanner : NSObject <CBCentralManagerDelegate>
@property (copy) NSUUID *proximityUUID;
@property (copy) NSNumber *major;
@property (copy) NSNumber *minor;
@property (strong, nonatomic) NSData *publicKey;

-(void)startScanningWithBlock:(void (^)(NSDictionary *))inScanningBlock errorBlock:(void (^)(NSString *error))inErrorBlock;
-(void)stopScanning;
-(void)resumeScanning;
@end
