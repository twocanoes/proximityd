//
//  TCLBeaconScanner.m
//  Bleu Broadcaster
//
//  Created by Tim Perfitt on 5/15/15.
//  Copyright (c) 2015 Twocanoes Software. All rights reserved.
//

#import <asl.h>
#import "TCLBeaconScanner.h"

@interface TCLBeaconScanner ()
@property (strong) CBCentralManager * manager;
@property (nonatomic, copy) void (^scanningBlock)(NSDictionary *);
@property (nonatomic, copy) void (^errorBlock)();
@property (strong,nonatomic) CBCentralManager *centralManager;
@property (nonatomic, strong) dispatch_queue_t managerQueue;

@property (strong) NSMutableArray *signingParts;
@end
@implementation TCLBeaconScanner



-(void)stopScanning{
    [self.manager stopScan];
    
}

-(void)resumeScanning{
    
    [self kickOffScanning];
}
-(void)startScanningWithBlock:(void (^)(NSDictionary *))inScanningBlock errorBlock:(void (^)(NSString *))inErrorBlock{
    
    self.managerQueue = dispatch_queue_create("com.twocanoes.centralManagerQueue", NULL);
    
    
    self.manager = [[CBCentralManager alloc] initWithDelegate:self
                                                        queue:self.managerQueue];
    
    self.scanningBlock=inScanningBlock;
    self.errorBlock=inErrorBlock;
    
    [self kickOffScanning];
}
-(void)centralManagerDidUpdateState:(CBCentralManager *)central{
    [self kickOffScanning];
}
-(void)kickOffScanning{
    
    if (self.manager.state==CBCentralManagerStatePoweredOff) {
        self.errorBlock(@"Bluetooth powered off");
    }
    else  if (self.manager.state==CBCentralManagerStateResetting) {
        self.errorBlock(@"Bluetooth Resetting");
    }
    else  if (self.manager.state==CBCentralManagerStateUnsupported) {
        self.errorBlock(@"Bluetooth Unsupported");
    }
    else  if (self.manager.state==CBCentralManagerStateUnauthorized) {
        self.errorBlock(@"Bluetooth Unauthorized");
    }
    else if (self.manager.state==CBCentralManagerStatePoweredOn) {
        [self.manager scanForPeripheralsWithServices:nil options:@{ CBCentralManagerScanOptionAllowDuplicatesKey : @NO}];
    }
    
}

- (void)centralManager:(CBCentralManager *)central
 didDiscoverPeripheral:(CBPeripheral *)peripheral
     advertisementData:(NSDictionary *)advertisementData
                  RSSI:(NSNumber *)RSSI{
    
    
    
    
    NSData *manufacturerData = [advertisementData objectForKey:CBAdvertisementDataManufacturerDataKey];
    
    unsigned const char preamble[2] = {0xff, 0xff};
    NSData *preambleData = [NSData dataWithBytes:preamble length:2];
    NSData *receivedHeader = [manufacturerData subdataWithRange:NSMakeRange(0, 2)];
    
    if ([receivedHeader isEqualToData:preambleData]) {
    
        //Trusted Beacon
    }
    
    

    else {
    
    
        NSData *advData=[advertisementData objectForKey:@"kCBAdvDataManufacturerData"];
        NSDictionary *serviceDataDict= (NSDictionary *)[advertisementData objectForKey:CBAdvertisementDataServiceDataKey];
        
        char beaconBytes[]={0x4c,0x00,0x02,0x15};
        
        uint16_t majorNumber;
        uint16_t minorNumber;
        int8_t powerNumber;
        if ([[advData subdataWithRange:NSMakeRange(0, 4)] isEqualToData:[NSData dataWithBytes:&beaconBytes length:4]]) {
            
            NSData *uuid=[advData subdataWithRange:NSMakeRange(4,16)];
            NSData *majorData=[advData subdataWithRange:NSMakeRange(20,2)];
            NSData *minorData=[advData subdataWithRange:NSMakeRange(22,2)];
            NSData *powerData=[advData subdataWithRange:NSMakeRange(24,1)];
            
            
            NSUUID *regionUUID=[[NSUUID alloc] initWithUUIDBytes:[uuid bytes]];
            
            majorNumber = CFSwapInt16BigToHost(*(int*)([majorData bytes]));
            minorNumber = CFSwapInt16BigToHost(*(int*)([minorData bytes]));
            [powerData getBytes:&powerNumber length:1];
            
            self.scanningBlock(@{@"Type":@"iBeacon",
                                 @"RegionUUID":regionUUID.UUIDString,
                                 @"MajorNumber":[NSString stringWithFormat:@"%i",majorNumber],
                                 @"MinorNumber":[NSString stringWithFormat:@"%i",minorNumber],
                                 @"PowerCalibration":[NSString stringWithFormat:@"%i",powerNumber],
                                 @"RSSI":[NSString stringWithFormat:@"%li",[RSSI integerValue]]
                                 });
        }
        
    
    }

}



@end
