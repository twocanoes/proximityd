//
//  NSDictionary+SingleLineDescription.h
//  beacond
//
//  Created by Tim Perfitt on 6/4/15.
//  Copyright (c) 2015 Twocanoes Labs. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDictionary (SingleLineDescription)
-(NSString *)singleLineDescription;
@end
