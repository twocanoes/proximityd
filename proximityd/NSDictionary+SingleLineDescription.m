//
//  NSDictionary+SingleLineDescription.m
//  beacond
//
//  Created by Tim Perfitt on 6/4/15.
//  Copyright (c) 2015 Twocanoes Labs. All rights reserved.
//

#import "NSDictionary+SingleLineDescription.h"

@implementation NSDictionary (SingleLineDescription)

-(NSString *)singleLineDescription{
    

    NSMutableString *mutableString=[NSMutableString string];
    
    [self enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
        [mutableString appendFormat:@"%@:%@,",key,obj];
    }];
    if (mutableString.length>0) [mutableString deleteCharactersInRange:NSMakeRange(mutableString.length-1, 1)];
    return [NSString stringWithString:mutableString];;
}
@end
