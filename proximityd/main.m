//
//  main.m
//  beacond
//
//  Created by Tim Perfitt on 5/19/15.
//  Copyright (c) 2015 Twocanoes Labs. All rights reserved.
//

 
#import <Foundation/Foundation.h>
#import "TCLBeaconScanner.h"
#import "TCLMainController.h"


int main(int argc, const char * argv[]) {
    @autoreleasepool {
        
        
        if ([[NSProcessInfo processInfo].arguments containsObject:@"-license"]) {
            NSLog(@"\nCopyright 2015, Twocanoes Labs, LLC.\n\
Contains uECC code Copyright 2014, Kenneth MacKay. Licensed under the folowing BSD 2-clause license.\n\
All rights reserved.\n\
\n\
Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:\n\
\n\
1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.\
\n\
2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.\n\
\n\
THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS \"AS IS\" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.\n\
");
            exit (0);
        }
        
        TCLMainController *controller=[[TCLMainController alloc] init];
        
        [controller start];
        
        
//        TCLBeaconScanner *scanner=[[TCLBeaconScanner alloc] init];
//        [scanner startScanningWithBlock:^(NSDictionary *dict) {
//            
//            
//        } errorBlock:^(NSString *error) {
//            NSLog(@"%@",error);
//            exit(-1);
//        }
//         ];
        
//        int scantime=0;
//        NSDate *now=[NSDate date];
//        
        
//        if ([ud objectForKey:@"ScanTime"]) scantime=[[ud objectForKey:@"ScanTime"] intValue];
        
//        while (true) {
//            if (scantime>0) {
//                sleep(1);
//                if (fabs([now timeIntervalSinceNow])>scantime) {
//                    exit(0);
//                    
//                }
//            }
//            else {
//                sleep(1);
//            }
//        }
        
        
        [[NSRunLoop currentRunLoop] run];
    }
    return 0;
}
