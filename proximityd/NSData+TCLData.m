//
//  NSData+TCLHash.m
//  Bleu Client
//
//  Created by Steve Brokaw on 4/9/15.
//  Copyright (c) 2015 Twocanoes Labs, LLC. All rights reserved.
//

#import "NSData+TCLData.h"
#import "CommonCrypto/CommonCrypto.h"

@implementation NSData (TCLHash)

- (NSString *)tcl_hexString
{
    return [self tcl_hexStringForward:YES];
}

- (NSString *)tcl_hexStringForward:(BOOL)isForward
{
    const unsigned char *bytes = (const unsigned char*)[self bytes];
    NSUInteger dataLen = [self length];
    NSUInteger strLen = 2 * dataLen;
    NSMutableString *hex = [[NSMutableString alloc] initWithCapacity:strLen];
    for(NSUInteger i = 0; i < dataLen; ++i) {
        if (isForward) {
            [hex appendFormat:@"%02x", bytes[i]];
        } else {
            NSString *hexByte = [NSString stringWithFormat:@"%02x", bytes[i]];
            [hex insertString:hexByte atIndex:0];
        }
    }
    return hex;
}

- (NSData *)tcl_sha1Hash
{
    unsigned char hash[CC_SHA1_DIGEST_LENGTH];
    CC_LONG len = (unsigned int)[self length];
    CC_SHA1([self bytes], len, hash);
    return [NSData dataWithBytes:hash length:CC_SHA1_DIGEST_LENGTH];
}

- (NSString *)tcl_sha1String
{
    return [[self tcl_sha1Hash] tcl_hexString];
}

- (NSData *)tcl_sha256Hash {
    unsigned char hash[CC_SHA256_DIGEST_LENGTH];
    CC_LONG len = (unsigned int)[self length];
    CC_SHA256([self bytes], len, hash);
    return [NSData dataWithBytes:hash length:CC_SHA256_DIGEST_LENGTH];
}

- (NSData *)tcl_md5Hash {
    unsigned char hash[CC_MD5_DIGEST_LENGTH];
    CC_LONG len = (unsigned int)[self length];
    CC_MD5([self bytes], len, hash);
    return [NSData dataWithBytes:hash length:CC_MD5_DIGEST_LENGTH];
}


@end

@implementation NSData (TCLScalar)

+ (NSData *)tcl_dataWithInt8:(int8_t)value
{
    return [self tcl_dataWithUint8:value];
}

+ (NSData *)tcl_dataWithUint8:(uint8_t)value
{
    return [NSData dataWithBytes:&value length:1];
}

+ (NSData *)tcl_dataWithUint16:(uint16_t)value
{
    value = CFSwapInt16(value);
    return [NSData dataWithBytes:&value length:2];
}

+ (NSData *)tcl_dataWithDouble:(double)value
{
    return [NSData dataWithBytes:&value length:8];
}

- (int8_t)tcl_int8Value
{
    return (int8_t)[self tcl_uint8Value];
}

- (uint8_t)tcl_uint8Value
{
    NSAssert([self length] == 1, @"Inconsistent data length for requested value.");
    uint8_t value = 0;
    [self getBytes:&value length:1];
    return value;
}

- (uint16_t)tcl_uint16Value
{
    NSAssert([self length] == 2, @"Inconsistent data length for requested value.");
    uint16_t value = 0;
    [self getBytes:&value length:2];
    value = CFSwapInt16(value);
    return value;
}

- (uint32_t)tcl_uint32value
{
    NSAssert([self length] == 4, @"Inconsistent data length for requested value.");
    uint32_t value = 0;
    [self getBytes:(uint8_t *)&value length:4];
    return value;
}

- (double)tcl_doubleValue
{
    NSAssert([self length] == 8, @"Inconsistent data length for requested value.");
    double value = 0;
    [self getBytes:&value length:8];
    return value;
}

@end

@implementation NSData (TCLObjects)

- (NSString *)tcl_hexString
{
    return [self tcl_hexStringForward:YES];
}

- (NSString *)tcl_hexStringForward:(BOOL)isForward
{
    const unsigned char *bytes = (const unsigned char*)[self bytes];
    NSUInteger dataLen = [self length];
    NSUInteger strLen = 2 * dataLen;
    NSMutableString *hex = [[NSMutableString alloc] initWithCapacity:strLen];
    for(NSUInteger i = 0; i < dataLen; ++i) {
        if (isForward) {
            [hex appendFormat:@"%02x", bytes[i]];
        } else {
            NSString *hexByte = [NSString stringWithFormat:@"%02x", bytes[i]];
            [hex insertString:hexByte atIndex:0];
        }
    }
    return hex;
}

- (NSString *)tcl_uuidStringValue
{
    NSUUID *uuid = [[NSUUID alloc] initWithUUIDBytes:[self bytes]];
    return [uuid UUIDString];
}

+ (instancetype)tcl_dataWithUUID:(NSUUID *)UUID
{
    uuid_t bytes;
    [UUID getUUIDBytes:bytes];
    return [[self class] dataWithBytes:bytes length:sizeof(uuid_t)];
}

+ (instancetype)tcl_dataWithUUIDString:(NSString *)value
{
    NSUUID *uuid = [[NSUUID alloc] initWithUUIDString:value];
    if (!uuid) return nil;
    return [[self class] tcl_dataWithUUID:uuid];
}


@end

@implementation NSData (TCLBleuStation)

- (NSString *)tcl_stringValue
{
    uint8_t length = 0;
    [self getBytes:&length length:1];
    NSRange range = NSMakeRange(1, length);
    NSAssert([self length] == length + 1, @"Unexpected string length");
    return [[NSString alloc] initWithData:[self subdataWithRange:range] encoding:NSUTF8StringEncoding];
}

+ (NSData *)tcl_dataWithString:(NSString *)value
{
    NSData *stringData = [value dataUsingEncoding:NSUTF8StringEncoding];
    uint8_t len = [stringData length];
    if (!len) return nil;
    NSMutableData *data = [[NSMutableData alloc] initWithBytes:&len length:1];
    [data appendData:stringData];
    return [NSData dataWithData:data];
}


- (NSString *)tcl_MACAddressString
{
    const unsigned char *bytes = (const unsigned char*)[self bytes];
    NSUInteger dataLen = [self length];
    NSUInteger strLen = 3 * dataLen;
    NSMutableString *hex = [[NSMutableString alloc] initWithCapacity:strLen];
    for(NSUInteger i = 0; i < dataLen; ++i) {
        NSString *hexByte = [NSString stringWithFormat:@"%02x:", bytes[i]];
        [hex insertString:hexByte atIndex:0];
    }
    [hex deleteCharactersInRange:NSMakeRange(strLen - 1, 1)];
    return hex;
}

- (uint16_t)tcl_crc16Checksum
{
    // CRC16
    // init val = 0x0000
    // poly: 0x8005

    uint16_t crc = 0;
    uint16_t poly = 0x8005;

    const uint8_t *bytes = [self bytes];
    NSUInteger len = [self length];
    for (NSUInteger num = len; num > 0; num--) {
        crc ^= ((*bytes++) << 8);
        for (int i = 0; i < 8; i++) {
            crc = crc <<1;
            if (crc & 0x10000) {
                crc = (crc ^ poly) & 0xFFFF;
            }
        }
    }
    return crc;
}

- (uint16_t)tcl_storedChecksumAtindex:(uint16_t)index
{
    const int len = 2;
    uint16_t stored_crc = 0;
    uint8_t bytes[len];
    [self getBytes:bytes range:NSMakeRange(index, index + len)];
    stored_crc = (bytes[1] << 8) + bytes[0];
    return stored_crc;
}

@end