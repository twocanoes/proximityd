//
//  TCLMainController.h
//  Beacon Launcher
//
//  Created by Tim Perfitt on 5/17/15.
//  Copyright (c) 2015 Twocanoes Labs. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface TCLMainController : NSObject
-(void)start;
-(NSString *)beaconId:(NSDictionary *)inBeaconDict;
@end
