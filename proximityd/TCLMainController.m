//
//  TCLMainController.m
//  Beacon Launcher
//
//  Created by Tim Perfitt on 5/17/15.
//  Copyright (c) 2015 Twocanoes Labs. All rights reserved.
//

#import "NSDictionary+SingleLineDescription.h"
#import <asl.h>
#import "TCLMainController.h"
#import "TCLBeaconScanner.h"
#define TCLOGDEBUG(format,...) { asl_log(NULL, NULL, ASL_LEVEL_DEBUG, "%s", [[NSString stringWithFormat:format, ##__VA_ARGS__] UTF8String]);\
}

#define TCLOGINFO(format,...) {asl_log(NULL, NULL, ASL_LEVEL_INFO, "%s", [[NSString stringWithFormat:format, ##__VA_ARGS__] UTF8String]); \
}
#define TCLOGWARNING(format,...) { asl_log(NULL, NULL, ASL_LEVEL_WARNING, "%s", [[NSString stringWithFormat:format, ##__VA_ARGS__] UTF8String]); \
}

@interface TCLMainController()

@property (strong) NSMutableDictionary *lastInRegionDict;
@property (strong) NSMutableDictionary *lastOutsideRegionDict;
@property (assign) BOOL inRegion;
@property (strong) TCLBeaconScanner *scanner;
@property (strong) NSTimer *checkTimer;
@end
@implementation TCLMainController
- (IBAction)getBeaconsButtonPressed:(id)sender {
        [[NSWorkspace sharedWorkspace] openURL:[NSURL URLWithString:@"http://store.twocanoes.com"]];
    

}

-(void)checkExit:(id)sender{
    TCLOGDEBUG(@"%@",@"Checking Exit");
    [self.lastInRegionDict  enumerateKeysAndObjectsUsingBlock:^(NSString* key, NSMutableDictionary *currDict, BOOL *stop) {
        TCLOGDEBUG(@"Checking %@ %@",key,currDict);
        if([[NSDate date] timeIntervalSinceDate:
            [[self.lastInRegionDict objectForKey:key] objectForKey:@"lastEnteredDate"]] > [self exitDelay]) {
            [self runExit:[[self.lastInRegionDict objectForKey:key] objectForKey:@"beacon"]];
            [self.lastInRegionDict removeObjectForKey:key];
            if ([self.lastInRegionDict count]==0) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self.checkTimer invalidate];
                });
            }

        }
        else {
            
            TCLOGDEBUG(@"Still in region: %@",currDict);
        }
        

    }];
    
}
-(NSString *)beaconId:(NSDictionary *)inBeaconDict{
    
    return [NSString stringWithFormat:@"%@:%@:%@",
            [[inBeaconDict objectForKey:@"RegionUUID"] uppercaseString],
            [[inBeaconDict objectForKey:@"MajorNumber"] uppercaseString],
            [[inBeaconDict objectForKey:@"MinorNumber"] uppercaseString]
            ];
}
-(int)exitDelay{
    
    int ret;
    NSUserDefaults *ud=[[NSUserDefaults alloc] initWithSuiteName:@"com.twocanoes.proximityd"];

    
    ret=20;
    
    if ([ud objectForKey:@"exitDelay"]) {
        ret=[[ud objectForKey:@"exitDelay"] intValue];
        
    }
    if (ret<5) ret=5;
    return ret;

}


-(void)start{
    TCLOGWARNING(@"Starting up");


    NSUserDefaults *ud=[[NSUserDefaults alloc] initWithSuiteName:@"com.twocanoes.proximityd"];
    
    TCLOGDEBUG(@"Exit Delay is %i",[self exitDelay]);
    self.lastInRegionDict=[NSMutableDictionary dictionary];//[NSDate distantPast];
    self.lastOutsideRegionDict=[NSMutableDictionary dictionary];//[NSDate distantPast];

    self.scanner=[[TCLBeaconScanner alloc] init];
    
    if ([ud objectForKey:@"PublicKey"]) {
        self.scanner.publicKey=[ud objectForKey:@"PublicKey"];
        TCLOGDEBUG(@"Got Public Key: %@",self.scanner.publicKey);
    }
    else {
        const unsigned char publicKeyBytes[] =
        {0xcc, 0xc5, 0x5d, 0xa7, 0x48, 0x87, 0xfd, 0x66, 0xa3, 0x6f, 0xda, 0x05,
            0x61, 0xd9, 0xc9, 0x79, 0x89, 0x24, 0xf1, 0xd4, 0xea, 0xc1, 0xfb, 0x44,
            0x42, 0xa3, 0x3e, 0x0f, 0x49, 0x40, 0xa5, 0xde, 0xb6, 0xc0, 0x2a, 0x49,
            0x13, 0x27, 0xa2, 0x4e};
        self.scanner.publicKey = [NSData dataWithBytes:publicKeyBytes length:sizeof(publicKeyBytes)];
    }
    if ([ud objectForKey:@"Regions"]) {
     
        TCLOGWARNING(@"Listening for %@",[ud objectForKey:@"Regions"]);
        
    }
    else {
        TCLOGWARNING(@"%@",@"No regions defined");

        
    }
    [self.scanner startScanningWithBlock:^(NSDictionary *dict) {
        
        NSUserDefaults *ud=[[NSUserDefaults alloc] initWithSuiteName:@"com.twocanoes.proximityd"];
        

        [[ud objectForKey:@"Regions"] enumerateObjectsUsingBlock:^(NSDictionary *beaconDict, NSUInteger idx, BOOL *stop) {
            BOOL matched=YES;
            if ([beaconDict objectForKey:@"RegionUUID"] &&
                ![[[beaconDict objectForKey:@"RegionUUID"] uppercaseString] isEqualToString:[[dict objectForKey:@"RegionUUID"] uppercaseString]]){
                matched=NO;
                TCLOGDEBUG(@"Did not match UUID %@ with %@",[[beaconDict objectForKey:@"RegionUUID"] uppercaseString],[[dict objectForKey:@"RegionUUID"] uppercaseString]);
            }
            
            if (matched == YES && [beaconDict objectForKey:@"MajorNumber"] &&
                [[beaconDict objectForKey:@"MajorNumber"] intValue] != [[dict objectForKey:@"MajorNumber"] intValue]){
                matched=NO;
                TCLOGDEBUG(@"Did not match MajorNumber %@ with %@",[beaconDict objectForKey:@"MajorNumber"] ,[dict objectForKey:@"MajorNumber"]);

            }
            
            if (matched == YES && [beaconDict objectForKey:@"MinorNumber"]  &&
                [[beaconDict objectForKey:@"MinorNumber"] intValue] != [[dict objectForKey:@"MinorNumber"] intValue]){
                matched=NO;
                TCLOGDEBUG(@"Did not match MinorNumber %@ with %@",[beaconDict objectForKey:@"MinorNumber"] ,[dict objectForKey:@"MinorNumber"]);

            }
            
            if (matched==YES) {
                TCLOGDEBUG(@"Matched %@", beaconDict);
//                if ([self.lastInRegionDict objectForKey:[self beaconId:dict]] ){
                if (![self.lastInRegionDict objectForKey:[self beaconId:dict]] ||
                    [[NSDate date] timeIntervalSinceDate:[[self.lastInRegionDict objectForKey:[self beaconId:dict]] objectForKey:@"lastEnteredDate"]] > [self exitDelay]) {
                    TCLOGDEBUG(@"Running with %@",beaconDict);
                        [self runEnter:beaconDict];
                }
                
                NSDictionary *newDateDict= @{@"lastEnteredDate":[NSDate date],@"beacon":beaconDict};
                [self.lastInRegionDict setObject:newDateDict forKey:[self beaconId:dict]];

            }
        }];
        


    } errorBlock:^(NSString *error) {
        
    }];
}

-(void)runEnter:(NSDictionary *)inDict{
    [self sendSystemNotification:@{@"description":[NSString stringWithFormat:@"Entering Region \"%@\"",inDict[@"RegionName"]],@"event":@"enter",@"beaconInfo":inDict}];

    
    TCLOGWARNING(@"Entering : %@",[inDict singleLineDescription]);
    
    if ([inDict objectForKey:@"runOnEnterCommand"]) {
        NSString *command=[inDict objectForKey:@"runOnEnterCommand"];
        [self runTask:command];
    }


        [self resetTimer];
   
    
}
-(void)runTask:(NSString *)inTask{
    TCLOGWARNING(@"Running command \"%@\"",inTask);
    NSTask *task=[NSTask launchedTaskWithLaunchPath:@"/bin/sh" arguments:@[@"-c",inTask]];
    [task waitUntilExit];

}
-(void)resetTimer{
    TCLOGDEBUG(@"TIMER RESET");

    dispatch_async(dispatch_get_main_queue(), ^{

        if ([self.checkTimer isValid]) [self.checkTimer invalidate];
        self.checkTimer=[NSTimer scheduledTimerWithTimeInterval:2 target:self selector:@selector(checkExit:) userInfo:nil repeats:YES];
    });

}
-(void)sendSystemNotification:(NSDictionary *)userInfo{
    
    CFNotificationCenterRef distributedCenter = CFNotificationCenterGetDarwinNotifyCenter();
    
    if (distributedCenter!=nil) {
        TCLOGDEBUG(@"Posting Notification %@",userInfo);
    
        NSString *statePath=@"/Library/Application Support/Twocanoes/Proximityd";
        NSError *err;
        if (![[NSFileManager defaultManager] fileExistsAtPath:statePath]) {
            BOOL ret=[[NSFileManager defaultManager] createDirectoryAtPath:statePath withIntermediateDirectories:YES attributes:nil error:&err];
            if (ret!=0) {
                TCLOGWARNING(@"Could not create dir: error %@",[err localizedDescription]);
            }
        }
        
        
        [userInfo writeToFile:@"/Library/Application Support/Twocanoes/Proximityd/state.plist" atomically:YES];
        CFNotificationCenterPostNotification(distributedCenter,
                                             CFSTR("com.twocanoes.proximityd"),
                                             CFSTR("com.twocanoes.proximityd"),
                                             (__bridge CFDictionaryRef)userInfo,
                                             true);
    }

}

-(void)runExit:(NSDictionary *)inDict{
    [self sendSystemNotification:@{@"description":[NSString stringWithFormat:@"Exiting Region \"%@\"",inDict[@"RegionName"]],@"event":@"exit",@"beaconInfo":inDict}];

    TCLOGWARNING(@"Exiting : %@",[inDict singleLineDescription]);
    if ([inDict objectForKey:@"runOnExitCommand"]) {
        NSString *command=[inDict objectForKey:@"runOnExitCommand"];

        [self runTask:command];
    }

}
@end
