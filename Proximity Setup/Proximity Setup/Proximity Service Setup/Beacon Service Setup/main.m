//
//  main.m
//  Beacon Service Setup
//
//  Created by Tim Perfitt on 5/25/15.
//  Copyright (c) 2015 Twocanoes Labs. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, const char * argv[]) {
    return NSApplicationMain(argc, argv);
}
