//
//  AppDelegate.h
//  Beacon Service Setup
//
//  Created by Tim Perfitt on 5/25/15.
//  Copyright (c) 2015 Twocanoes Labs. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface AppDelegate : NSObject <NSApplicationDelegate, NSTableViewDelegate>
- (IBAction)saveConfigProfile:(id)sender;
- (IBAction)editButtonPressed:(id)sender;
- (IBAction)cancelButtonPressed:(id)sender;
- (IBAction)addButtonPressed:(id)sender;
@property (weak) IBOutlet NSArrayController *arrayController;

- (IBAction)applyButtonPressed:(id)sender;

@end

