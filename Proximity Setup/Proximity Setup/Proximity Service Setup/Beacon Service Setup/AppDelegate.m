//
//  AppDelegate.m
//  Beacon Service Setup
//
//  Created by Tim Perfitt on 5/25/15.
//  Copyright (c) 2015 Twocanoes Labs. All rights reserved.
//

#import "AppDelegate.h"

@interface AppDelegate ()
@property (weak) IBOutlet NSWindow *detailsWindow;

@property (weak) IBOutlet NSWindow *window;
@end

@implementation AppDelegate

static void Callback(CFNotificationCenterRef center,
                     void *observer,
                     CFStringRef name,
                     const void *object,
                     CFDictionaryRef userInfo)
{
    NSLog(@"received event %@",[(__bridge NSDictionary *)userInfo description]);
}
- (void)applicationDidFinishLaunching:(NSNotification *)aNotification {
    
    [self resetToPrefs];
    CFNotificationCenterRef distributedCenter =
    CFNotificationCenterGetDistributedCenter();
    
    CFNotificationSuspensionBehavior behavior =
    CFNotificationSuspensionBehaviorDeliverImmediately;
    
    CFNotificationCenterAddObserver(distributedCenter,
                                    NULL,
                                    Callback,
                                    CFSTR("com.twocanoes.proximityd"),
                                    NULL,
                                    behavior);

    
}

-(void)resetToPrefs{
    [self.arrayController removeObjects:[self.arrayController arrangedObjects]];
    NSUserDefaults *ud=[NSUserDefaults standardUserDefaults];
    
    [[ud objectForKey:@"Regions"] enumerateObjectsUsingBlock:^(NSDictionary *dict, NSUInteger idx, BOOL *stop) {
        [self.arrayController addObject:[dict mutableCopy]];
        
    }];

}
- (void)applicationWillTerminate:(NSNotification *)aNotification {
    // Insert code here to tear down your application
}

-(void)showEditSheet{
    [self.window beginSheet:self.detailsWindow completionHandler:^(NSModalResponse returnCode) {
        
    }];

}
-(void)tableDoubleClick:(id)sender{
    if (self.arrayController.selectionIndexes.count>0)    [self showEditSheet];
}

- (IBAction)saveConfigProfile:(id)sender {
    NSSavePanel *sp=[NSSavePanel savePanel];
    [sp setAllowedFileTypes:[NSArray arrayWithObject:@"mobileconfig"]];
    
    [sp beginSheetModalForWindow:nil completionHandler:^(NSInteger result){
        
        NSFileManager *fm=[NSFileManager defaultManager];
        if (result==NSModalResponseCancel) {
            return;
        }
        NSURL *saveURL=[sp URL];
        if ([fm fileExistsAtPath:[saveURL path]]) {
            NSError *err;
            if([fm removeItemAtPath:[saveURL path] error:&err]==NO) {
                NSRunAlertPanel(NSLocalizedString(@"Error",@"NSRunAlertPanel title text removing file"),
                                NSLocalizedString(@"Failed deleting file %@.",@"alert"),
                                NSLocalizedString(@"OK",@"NSRunAlertPanel button text when restore fails"),
                                nil,
                                nil,
                                [saveURL path]);
                return;
            }
        }
        
        
        NSUserDefaults *ud=[NSUserDefaults standardUserDefaults];
        NSMutableDictionary *beaconDict=[[NSDictionary dictionaryWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"proximityd" ofType:@"plist" ]] mutableCopy];
        
        beaconDict[@"PayloadContent"][0][@"PayloadContent"][@"com.twocanoes.proximityd"][@"Forced"][0][@"mcx_preference_settings"][@"Regions"]=[self.arrayController arrangedObjects];
       
        if ([ud objectForKey:@"exitDelay"]) {
            
            beaconDict[@"PayloadContent"][0][@"PayloadContent"][@"com.twocanoes.proximityd"][@"Forced"][0][@"mcx_preference_settings"][@"exitDelay"]=[ud objectForKey:@"exitDelay"];
        }

        
        [beaconDict writeToFile:[saveURL path] atomically:NO];
        
    }];

    
    
}

- (IBAction)editButtonPressed:(id)sender {
        [self showEditSheet];
}

- (IBAction)cancelButtonPressed:(id)sender {
    [self resetToPrefs];
    [self.window endSheet:self.detailsWindow returnCode:0];

}

- (IBAction)addButtonPressed:(id)sender {
    [self.arrayController add:self];
     [self showEditSheet];

}
- (IBAction)applyButtonPressed:(id)sender {
    
    NSMutableDictionary *regionObject=[[self.arrayController selectedObjects] firstObject] ;
    NSString * uuidString=[regionObject objectForKey:@"RegionUUID"];
    NSUUID *uuid=[[NSUUID alloc] initWithUUIDString:uuidString];
    
    if (uuid==nil || !uuidString|| [uuidString length]==0) {
        
        NSAlert *alert=[[NSAlert alloc] init];
        alert.messageText=@"Invalid UUID";
        alert.informativeText=@"The UUID you provided is not a valid UUID. Please enter in a valid UUID.";
        [alert runModal];
    }
    else {
    
        
        NSUserDefaults *ud=[NSUserDefaults standardUserDefaults];
        [ud setObject:[self.arrayController arrangedObjects] forKey:@"Regions"];
        [self.window endSheet:self.detailsWindow returnCode:0];
    }
}
@end
