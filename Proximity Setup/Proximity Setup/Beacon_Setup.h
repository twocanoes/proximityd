//
//  Beacon_Setup.h
//  Beacon Setup
//
//  Created by Tim Perfitt on 5/25/15.
//  Copyright (c) 2015 Twocanoes Labs. All rights reserved.
//

#import <PreferencePanes/PreferencePanes.h>

@interface Beacon_Setup : NSPreferencePane

- (void)mainViewDidLoad;

@end
