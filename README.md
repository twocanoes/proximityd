![2525187913-icon_128x128.png](https://bitbucket.org/repo/x64xLn/images/2069500087-2525187913-icon_128x128.png)
# Getting Started With proximityd #

Manage your Macs using iBeacons! Run a command, install a package, enforce a policy just by having the Mac in range of a beacon.  Just install the package and you can change any setting, install any package, apply any policy based on proximity to any iBeacon.

Proximityd is a daemon that runs on OS X.  Once installed, it that listens for beacons and automatically runs scripts in the background based on rules you set up.  All configuration is done via Configuration Profiles, so setup is a breeze.  

## Getting Started ##

First, [purchase iBeacons at store.twocanoes.com](http://store.twocanoes.com/collections/ibeacons).  Our beacons (and this project) will support Trusted Beacons, which means that you can know that your iBeacon is not being spoofed.  You can use the beacons in standard mode as well and proximityd will work fine with it.

In the [downloads section](https://bitbucket.org/twocanoes/proximityd/downloads), grab the latest DMG.  Open the disk image and double click on the installer, and following items will be installed:

* /Applications/Utilities/Proximity Service Setup.app: Configuration app for generating a configuration profile for use with the daemon.
 
* /Library/PrivilegedHelperTools/proximityd: This is the daemon that gets launched.
 
* /Library/LaunchDaemons/com.twocanoes.proximityd.plist: The configuration file to start up the agent.

The installer will start up the daemon so no restart is required.

## Configuration ##
All the configuration is done via configuration profiles, either locally or via a MDM server.  A sample profile, called “3PackConfig.mobileconfig” is included as an example.  When you install this configuration profile, it will do 3 things when leaving/entering 3 beacon ranges:


### INSTALL PRINTER: ###
UUID:E2C56DB5-DFFB-48D2-B060-D0F5A71096E0 MajorNumber:1 MinorNumber:3

Enter Command:

lpadmin -p DemoInkJetFaxPrinter -v socket://19.86.82.172 -P '/Library/Printers/PPDs/Contents/Resources/hp color LaserJet 2500.gz'

Exit Command:

lpadmin -x DemoInkJetFaxPrinter

### DISABLE ADMIN ###
UUID:E2C56DB5-DFFB-48D2-B060-D0F5A71096E0 MajorNumber:1 MinorNumber:4

Enter Command:

profiles -R -F /Library/Application\ Support/Twocanoes/turnoffsysprefs.mobileconfig ; profiles -I -F /Library/Application\ Support/Twocanoes/sysprefdock.mobileconfig 

Exit Command:

profiles -I -F /Library/Application\ Support/Twocanoes/turnoffsysprefs.mobileconfig ; profiles -R -F /Library/Application\ Support/Twocanoes/sysprefdock.mobileconfig</string>


### ACTIVATE SCREENSAVER ###
UUID:E2C56DB5-DFFB-48D2-B060-D0F5A71096E0 MajorNumber:1 MinorNumber:2

Enter Command:NONE

Exit Command:

open /System//Library/Frameworks/ScreenSaver.framework/Versions/A/Resources/ScreenSaverEngine.app

## Creating your own profiles ##
To create your own configuration profiles, open the configuration app in Utilities called Proximity Service Setup:

![3364954657-pasted-image-16.png](https://bitbucket.org/repo/x64xLn/images/1959908163-3364954657-pasted-image-16.png)

![82569439-pasted-image-19.png](https://bitbucket.org/repo/x64xLn/images/4262723042-82569439-pasted-image-19.png)
When done, click Save Config Profile, and then install that config profile on any Mac that has proximityd installed and the settings will immediately take affect.